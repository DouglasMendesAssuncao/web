import React from 'react';
import '../app/helpers/Global.css';

import Routes from './routes'

function App() {
  return (
    <Routes />
  );
}

export default App;
