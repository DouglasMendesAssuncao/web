import styled from 'styled-components'

export const Title = styled.div`
  margin-top: 3rem;
  font-family: 'Roboto';
  font-size: 2.9rem;
  font-weight: 700;
  color: var(--text-primary);
`

export const SubTitle = styled.div`
  margin-top: 3rem;
  margin-bottom: 3rem;
  text-align: left;
  font: 400 1.9rem Roboto;
  color: var(--text-second);
  `
